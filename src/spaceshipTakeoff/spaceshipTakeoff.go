package main;

import (
    "fmt"
    "math/rand"
    "time"
);

const WIN = true;
const LOOSE = false;

func main() {
    fmt.Println("Spaceship Takeoff");

    if( game() == WIN ) {
        fmt.Println("Good take off!");
    } else {
        fmt.Println("You failed -");
        fmt.Println("The aliens got you!");
    }
}

func initialise() (int, int) {
    rand.Seed(time.Now().UnixNano());
    var gravity = rand.Intn(20) + 1;
    var weight = rand.Intn(40) + 1;

    return gravity, gravity*weight;
}

func game() bool {
    var gravity, target int = initialise();

    for turn := 0; turn < 10; turn++ {
        var force = 0;
        fmt.Println("Planet gravity is ", gravity); 
        fmt.Print("Enter amount of force to use:");
        fmt.Scan(&force);

        if( force > target ) {
            fmt.Println("Too much force");
        } else if( force < target ) {
            fmt.Println("Too little force");
        } else {
            return WIN;
        }
    }

    return LOOSE;
}
