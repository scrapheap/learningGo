package main;

import (
    "fmt"
    "math"
    "math/rand"
    "time"
);

const WIN = true;
const LOOSE = false;

func main() {
    fmt.Println("Intergalactic Games");

    if( game() == WIN ) {
        fmt.Println("You've done it!");
        fmt.Println("NCTV wins - thanks to you!");
    } else {
        fmt.Println("You failed -");
        fmt.Println("You're fired!");
    }
}

func initialise() int {
    rand.Seed(time.Now().UnixNano());
    var height = rand.Intn(100) + 1;

    return height;
}

func game() bool {
    var height int = initialise();

    for turn := 0; turn < 10; turn++ {
        var angle = 0;
        var speed = 0;
        fmt.Println("You must launch a satalite to a height of ", height); 
        fmt.Print("Enter angle to launch at:");
        fmt.Scan(&angle);
        fmt.Print("Enter speed to launch at:");
        fmt.Scan(&speed);

        var angleDelta = float64(angle) - math.Atan( float64(height) / 3.0 ) * 180.0 / math.Pi
        var speedDelta = float64(speed) - 3000.0 * math.Sqrt( float64(height) + 1.0 / float64(height) )

        if (math.Abs(angleDelta) < 2 && math.Abs(speedDelta) < 100 ) {
            return WIN;
        }

        if( angleDelta < -2 ) {
            fmt.Println("Too shallow");
        } else if( angleDelta > 2 ) {
            fmt.Println("Too steep");
        }

        if( speedDelta < -100 ) {
            fmt.Println("Too slow");
        } else if( speedDelta > 100 ) {
            fmt.Println("Too fast");
        }
    }

    return LOOSE;
}
