GO=go

.PHONY: all
all: test build

.PHONY: test
test:
	$(GO) test -v ./...

.PHONY: build
build: bin/helloWorld bin/spaceshipTakeoff bin/intergalacticGames

bin/helloWorld: src/helloWorld/helloWorld.go
	$(GO) build -o $@ -v $^

bin/spaceshipTakeoff: src/spaceshipTakeoff/spaceshipTakeoff.go
	$(GO) build -o $@ -v $^

bin/intergalacticGames: src/intergalacticGames/intergalacticGames.go
	$(GO) build -o $@ -v $^
