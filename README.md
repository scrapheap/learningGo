# Learning Go

These small codebases are simple ones I've created while learning the basics of
Go.  These are all based on BASIC games in the
[Usborne Computer Spacegames](https://usborne.com/browse-books/features/computer-and-coding-books/)
book.

## Starship Takeoff
You've got to launch your spaceship into orbit and you've only got 10 goes. A
rough flowchart for the game looks like the following:

### Original flowchart
```mermaid
graph TD
WELCOME[Display welcome message]
--> SETUP[Setup game state]
--> DISPLAY_INFO[Tell user current<br/>information]
--> GET_FORCE[Get the force to<br/>try from user]
--> TOO_MUCH{Too much force?}
-->|No| TOO_LITTLE{Too little force?}
-->|No| JUST_RIGHT{Just the right<br/>amount of force?}
-->|No| TURNS_LEFT{Does the player<br/>have any turns left?}
-->|No| YOU_LOSE[Tell the user<br/>they've lost]
--> GAME_OVER[Game Over]

TOO_MUCH -->|Yes| DISPLAY_TOO_MUCH[Tell the user their<br/>force was too high]
--> TOO_LITTLE

TOO_LITTLE -->|Yes| DISPLAY_TOO_LITTLE[Tell the user their<br/>force was too low]
--> JUST_RIGHT

TURNS_LEFT -->|Yes| GET_FORCE

JUST_RIGHT -->|Yes| YOU_WIN[Tell the user<br/>they've won]
--> GAME_OVER
```

## Intergalactic Games
Launch your satalite into orbit before your competitors have launched theirs.

### Original flowchart
```mermaid
graph TD
WELCOME[Display welcome message]
--> SETUP[Setup game state]
--> DISPLAY_INFO[Tell user target<br/>information]
--> GET_ANGLE[Get the Angle to fire<br/>at from user]
--> GET_SPEED[Get the Speed to fire<br/>at from user]
--> CALCULATE_DELTAS[Calculate the difference between<br/>the required angles and speed]
--> WITHIN_THRESHOLD{Are the differences within<br/>the threshold for success}
--> |No| TOO_SHALLOW{Is the angle<br/>too shallow?}
--> |No| TOO_STEEP{Is the angle<br/>too steep?}
--> |No| TOO_SLOW{Is the speed<br/>too slow?}
--> |No| TOO_FAST{Is the speed<br/>too fast?}
--> |No| TURNS_LEFT{Does the player<br/>have any turns left?}
--> |No| YOU_LOSE[Tell the user<br/>they've lost]
--> GAME_OVER[Game Over]

WITHIN_THRESHOLD -->|Yes| YOU_WIN[Tell the user<br/>they've won]
--> GAME_OVER

TOO_SHALLOW --> |Yes| DISPLAY_TOO_SHALLOW[Tell the user their<br/>angle was too shallow]
--> TOO_STEEP
--> |Yes| DISPLAY_TOO_STEEP[Tell the user their<br/>angle was too steep]
--> TOO_SLOW
--> |Yes| DISPLAY_TOO_SLOW[Tell the user their<br/>speed was too slow]
--> TOO_FAST
--> |Yes| DISPLAY_TOO_FAST[Tell the user their<br/>speed was too fast]
--> TURNS_LEFT

TURNS_LEFT -->|Yes| GET_ANGLE

```
